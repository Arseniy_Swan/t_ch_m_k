#include <iostream>
#include <string>
#include "longNumber.h"

using namespace std;

int main(int argc, const char * argv[]) {
    
    bool binary = false;
    /*
    argc = 6;
    argv[1] = "/Users/admin/Xcode projects/longArithmetics2/longArithmetics2/1";
    argv[2] = "-";
    argv[3] = "/Users/admin/Xcode projects/longArithmetics2/longArithmetics2/3";
    argv[4] = "/Users/admin/Xcode projects/longArithmetics2/longArithmetics2/4";
    argv[5] = "-b";
    */
    

    if (argc < 5) {
        cout << "Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n" << endl;
        return 0;
    }
    
    string operation = argv[2];
    if ((operation.size() > 1 || operation[0] == '\0') || operation[0] != '+' && operation[0] != '-' && operation[0] != '*' && operation[0] != '/' && operation[0] != '%' && operation[0] != '^') {
        cout << "Error: Wrong operation: " << operation << endl;
        return 0;
    }
    
    if (operation[0] != '^') {
        if (argc < 5) {
            cout << "Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n" << endl;
            return 0;
        }
        
        if (argc > 6) {
            cout << "Error: Too many arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n" << endl;
            return 0;
        }
    }
    else {
        if (argc < 6) {
            cout << "Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n" << endl;
            return 0;
        }
        
        if (argc > 7) {
            cout << "Error: Too many arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n" << endl;
            return 0;
        }
    }
    
    const char* firstLongNumberFileName = argv[1];
    FILE* firstLongNumberFile = fopen(firstLongNumberFileName, "r");
    if (!firstLongNumberFile) {
        cout << "Error: Unable to open file: " << firstLongNumberFileName << endl;
        return 0;
    }
    
    const char* secondLongNumberFileName = argv[3];
    FILE* secondLongNumberFile = fopen(secondLongNumberFileName, "r");
    if (!secondLongNumberFile) {
        cout << "Error: Unable to open file: " << secondLongNumberFileName << endl;
        return 0;
    }
    
    const char* thirdLongNumberFileName = nullptr;
    FILE* thirdLongNumberFile = nullptr;
    if (operation[0] == '^') {
        thirdLongNumberFileName = argv[4];
        thirdLongNumberFile = fopen(thirdLongNumberFileName, "r");
        if (!thirdLongNumberFile) {
            cout << "Error: Unable to open file: " << thirdLongNumberFileName << endl;
            return 0;
        }
    }
    
    const char* resultLongNumberFileName = (operation[0] != '^') ? argv[4] : argv[5];
    FILE* resultLongNumberFile = fopen(resultLongNumberFileName, "w+");
    if (!resultLongNumberFile) {
        cout << "Error: Unable to open file: " << resultLongNumberFileName << endl;
        return 0;
    }
    
    string ifBinary = (operation[0] != '^') ? argv[5] : argv[6];
    if (argc == 6 && operation[0] != '^' || argc == 7 && operation[0] == '^') {
        if (ifBinary != "-b") {
            cout << "Error: Invalid flag: " << ifBinary << endl;
            return 0;
        }
        binary = true;
    }
    
    longNumber firstLongNumber(firstLongNumberFileName , firstLongNumberFile, binary);
    longNumber secondLongNumber(secondLongNumberFileName , secondLongNumberFile, binary);
    longNumber resultLongNumber; //(resultLongNumberFileName , resultLongNumberFile, binary);
    
    switch (operation[0]) {
        case '+':
        {
            resultLongNumber = firstLongNumber + secondLongNumber;
            break;
        }
        case '-':
        {
            resultLongNumber = firstLongNumber - secondLongNumber;
            break;
        }
        case '*':
        {
            resultLongNumber = firstLongNumber * secondLongNumber;
            break;
        }
        case '/':
        {
            resultLongNumber = firstLongNumber / secondLongNumber;
            break;
        }
        case '%':
        {
            resultLongNumber = firstLongNumber % secondLongNumber;
            break;
        }
        case '^':
        {
            longNumber thirdLongNumber(thirdLongNumberFileName , thirdLongNumberFile, binary);
            resultLongNumber = firstLongNumber.powerByModule(secondLongNumber, thirdLongNumber);
            break;
        }
        default:
            break;
    }
    /*
    //save result long number----------------------------------------------------------------------------------------
    saveLongNumber(resultLongNumberSize, resultLongNumber, resultLongNumFile);
    
    //free allocated memory and exit---------------------------------------------------------------------------------
    free(firstLongNumber);
    free(secondLongNumber);
    free(resultLongNumber);
    return 0;

    
    */
   
    
    resultLongNumber.save(resultLongNumberFile);
    
    
    return 0;
}
