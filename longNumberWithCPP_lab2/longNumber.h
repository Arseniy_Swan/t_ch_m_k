#ifndef __longArithmetics2__longNumber__
#define __longArithmetics2__longNumber__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


class longNumber {
public:
    longNumber() : _longNumberArray(nullptr), _longNumberSize(0), _longNumberFileName(nullptr), _longNumberFile(nullptr), _binary(false), _cellCapasity(1000000000) {};
    
    longNumber(const char *fileName, FILE* file, bool binary) : _longNumberArray(nullptr), _longNumberSize(0), _longNumberFileName(fileName), _longNumberFile(file), _binary(binary), _cellCapasity(1000000000) {
    
        _longNumberArray = loadLongNumber(&_longNumberSize, _longNumberFile, _longNumberFileName);
    };
    
    longNumber(unsigned long long *longNumberArray, unsigned long long longNumberSize, bool binary) : _longNumberArray(nullptr), _longNumberSize(longNumberSize), _longNumberFileName(nullptr), _longNumberFile(nullptr), _binary(binary), _cellCapasity(1000000000) {
        
        _longNumberArray = (unsigned long long*)malloc(_longNumberSize * sizeof(unsigned long long));
        for (unsigned long long i = 0; i < _longNumberSize; ++i) {
            _longNumberArray[i] = longNumberArray[i];
        }
        
    };
    
    ~longNumber() {
        
        if (_longNumberFile) {
            fclose(_longNumberFile);
        }
        
        free(_longNumberArray);
        _longNumberArray = nullptr;
        _longNumberSize = 0;
        
        _longNumberFile = nullptr;
        _longNumberFileName = nullptr;
        
        _binary = false;
        _cellCapasity = 0;
    };
    
    
    longNumber& operator= (const longNumber &anotherLongNumber) {
        
        _binary = anotherLongNumber._binary;
        _longNumberSize = anotherLongNumber._longNumberSize;
        
        free(_longNumberArray);
        _longNumberArray = (unsigned long long*)malloc(_longNumberSize * sizeof(unsigned long long));
        
        for (unsigned long long i = 0; i < _longNumberSize; ++i) {
            _longNumberArray[i] = anotherLongNumber._longNumberArray[i];
        }
        
        return *this;
    }
    
    longNumber operator+ (longNumber &anotherLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = sum(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, anotherLongNumber._longNumberArray, anotherLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    longNumber operator- (longNumber &anotherLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = subtract(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, anotherLongNumber._longNumberArray, anotherLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    longNumber operator* (longNumber &anotherLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = multiply(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, anotherLongNumber._longNumberArray, anotherLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    longNumber operator/ (longNumber &anotherLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = divide(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, anotherLongNumber._longNumberArray, anotherLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    longNumber operator% (longNumber &anotherLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = module(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, anotherLongNumber._longNumberArray, anotherLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    longNumber powerByModule(longNumber &powerLongNumber, longNumber &moduleLongNumber) {
        
        unsigned long long *resultLongNumber = nullptr;
        unsigned long long resultLongNumberSize = 0;
        
        resultLongNumber = power(&resultLongNumberSize, this->_longNumberArray, this->_longNumberSize, powerLongNumber._longNumberArray, powerLongNumber._longNumberSize, moduleLongNumber._longNumberArray, moduleLongNumber._longNumberSize);
        
        return longNumber(resultLongNumber, resultLongNumberSize, this->_binary);
    }
    
    void save() {
        
        saveLongNumber(this->_longNumberSize , this->_longNumberArray, this->_longNumberFile);
    }
    
    void save(FILE* longNumberFile) {
        
        saveLongNumber(this->_longNumberSize , this->_longNumberArray, longNumberFile);
    }
    
private:
    unsigned long long *_longNumberArray;
    unsigned long long _longNumberSize;
    
    FILE* _longNumberFile;
    const char *_longNumberFileName;
    
    bool _binary;
    unsigned long long _cellCapasity;
    
    unsigned long long createAnElement(FILE* inputFile, unsigned short numOfDigits);
    unsigned long long* loadLongNumber(unsigned long long* longNumSize, FILE* longNumFile, const char* longNumFileName);
    void saveLongNumber(unsigned long long longNumSize, unsigned long long* longNum, FILE* longNumFile);
    
    unsigned long long* lvlUp(unsigned long long* longNumber, unsigned long long* longNumSize);
    
    unsigned char isLessOrEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned char isEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    
    unsigned long long* multiplyOnN(unsigned long long *resultLongNumberSize, unsigned long long* longNumber, unsigned long long longNumSize, unsigned long long n);
    
    unsigned long long* sum(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned long long* subtract(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned long long* multiply(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned long long* divide(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned long long* module(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);
    unsigned long long* power(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize, unsigned long long* thirdLongNumber, unsigned long long thirdLongNumberSize);

};



#endif /* defined(__longArithmetics2__longNumber__) */
