#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

const unsigned long long cellCapasity = 1000000000;
unsigned char binary = 0;

unsigned long long createAnElement(FILE* inputFile, unsigned short numOfDigits)
{
    unsigned long long element = 0;
    unsigned int pow10  = pow(10, numOfDigits - 1);
    char digit = '\0';
    for (unsigned short i = 0; i < numOfDigits; ++i) {
        digit = getc(inputFile);
        element += atoi(&digit) * pow10;
        pow10 /= 10;
    }
    return element;
}

unsigned long long* loadLongNumber(unsigned long long* longNumSize, FILE* longNumFile, const char* longNumFileName)
{
    if (binary) {
        fclose(longNumFile);
        longNumFile = fopen(longNumFileName, "rb");
        
        unsigned long long numOfBytes = 0;
        while (getc(longNumFile) != EOF) {
            ++numOfBytes;
        }
        
        *longNumSize = numOfBytes / 8;
        if (numOfBytes % 8 != 0) {
            ++*longNumSize;
        }
        
        unsigned long long* longNum = (unsigned long long*)malloc(*longNumSize * sizeof(unsigned long long));
        if (longNum == NULL) {
            printf("Error: Unable to allocate memory \n");
            exit(0);
        }
        memset(longNum, 0, *longNumSize * sizeof(unsigned long long));
        
        fseek(longNumFile, SEEK_SET, 0);
        
        unsigned long long tmp = 0;
        unsigned char byte = 0;
        unsigned long long j = 0;
        for (unsigned long long i = 0; i < *longNumSize; ++i) {
            do {
                fread(&byte, sizeof(unsigned char), 1, longNumFile);
                byte = (byte * 0x0202020202ULL & 0x010884422010ULL) % 1023;
                tmp ^= byte;
                tmp <<= 8 * (j % 8);
                longNum[i] ^= tmp;
                tmp = 0;
                ++j;
            } while (j % 8 != 0 && j != numOfBytes);
            
            tmp = longNum[i];
            tmp = 0;
        }
        
        fclose(longNumFile);
        return longNum;
        
    }
    else{
        unsigned long long numOfDigitsInLongNum = 0;
        while (getc(longNumFile) != EOF) {
            ++numOfDigitsInLongNum;
        }
        
        *longNumSize = numOfDigitsInLongNum / 9;
        if (numOfDigitsInLongNum % 9 != 0) {
            ++*longNumSize;
        }
        
        unsigned long long* longNum = (unsigned long long*)malloc(*longNumSize * sizeof(unsigned long long));
        if (longNum == NULL) {
            printf("Error: Unable to allocate memory \n");
            exit(0);
        }
        memset(longNum, 0, *longNumSize * sizeof(unsigned long long));
        
        unsigned long long longNumIndex = *longNumSize - 1;
        fseek(longNumFile, SEEK_SET, 0);
        if (numOfDigitsInLongNum % 9 != 0) {
            longNum[longNumIndex] = createAnElement(longNumFile, numOfDigitsInLongNum % 9);
            --longNumIndex;
        }
        
        
        for (; longNumIndex < ULONG_LONG_MAX; --longNumIndex) {
            longNum[longNumIndex] = createAnElement(longNumFile, 9);
        }
        
        fclose(longNumFile);
        return longNum;
    }
    
}

void saveLongNumber(unsigned long long longNumSize, unsigned long long* longNum, FILE* longNumFile) {
    if (binary) {
        
        unsigned long long tmp = 0;
        unsigned char byte = 0;
        unsigned char lastElement[8];
        unsigned char lastNonZeroByte = 0;
        for (unsigned long long i = 0; i < longNumSize; ++i) {
            tmp = longNum[i];
            if (i != longNumSize - 1) {
                for (unsigned char j = 0; j < 8; ++j) {
                    byte = tmp;
                    byte = (byte * 0x0202020202ULL & 0x010884422010ULL) % 1023;
                    fwrite(&byte, sizeof(unsigned char), 1, longNumFile);
                    
                    tmp >>= 8;
                }
            }
            else {
                for (unsigned char j = 0; j < 8; ++j) {
                    byte = tmp;
                    byte = (byte * 0x0202020202ULL & 0x010884422010ULL) % 1023;
                    lastElement[j] = byte;
                    if (byte != 0x00) {
                        lastNonZeroByte = j;
                    }
                    tmp >>= 8;
                }
                
                for (unsigned char j = 0; j < lastNonZeroByte + 1; ++j) {
                    fwrite(&lastElement[j], sizeof(unsigned char), 1, longNumFile);
                }
            }
        }
        
    }
    else {
        unsigned short numOfDigits = 0;
        for (unsigned long long i = longNumSize - 1; i < ULONG_LONG_MAX; --i) {
            if (i != longNumSize - 1) {
                numOfDigits = (unsigned short)log10(longNum[i]) + 1;
                for (unsigned short j = 0; j < 9 - numOfDigits; ++j) {
                    fprintf(longNumFile, "0");
                }
            }
            fprintf(longNumFile, "%llu", longNum[i]);
        }
    }
    fclose(longNumFile);
}

unsigned long long* lvlUp(unsigned long long* longNumber, unsigned long long* longNumSize);

unsigned char isLessOrEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned char isEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* multiplyOnN(unsigned long long *resultLongNumberSize, unsigned long long* longNumber, unsigned long long longNumSize, unsigned long long n);


unsigned long long* sum(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* subtract(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* multiply(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* divide(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* module(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize);

unsigned long long* power(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize, unsigned long long* thirdLongNumber, unsigned long long thirdLongNumberSize);

int main(int argc, const char * argv[]) {
    
    //initialize parameters, files and etc------------------------------------------------------------------
    if (argc < 3) {
        printf("Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n");
        return 0;
    }
    
    const char* operation = argv[2];
    if ((strlen(operation) > 1 || operation[0] == '\0') || operation[0] != '+' && operation[0] != '-' && operation[0] != '*' && operation[0] != '/' && operation[0] != '%' && operation[0] != '^') {
        printf("Error: Wrong operation: %s \n", operation);
        return 0;
    }
    
    if (operation[0] != '^') {
        if (argc < 5) {
            printf("Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n");
            return 0;
        }
        
        if (argc > 6) {
            printf("Error: Too many arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n");
            return 0;
        }
    }
    else {
        if (argc < 6) {
            printf("Error: Too few arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n");
            return 0;
        }
        
        if (argc > 7) {
            printf("Error: Too many arguments. <first_long_number_filename> [ + (sum) | - (subtract) | * (multiply) | / (divide) | %% (module) | ^ (power) ] <second_long_number_filename> [<third_long_number_filename> (only for power operation) ] <result_long_number_filename> [ -b ]\n");
            return 0;
        }
        
        
    }
    
    const char* firstLongNumFileName = argv[1];
    FILE* firstLongNumFile = fopen(firstLongNumFileName, "r");
    if (!firstLongNumFile) {
        printf("Error: Unable to open file: %s \n", firstLongNumFileName);
        return 0;
    }
    
    const char* secondLongNumFileName = argv[3];
    FILE* secondLongNumFile = fopen(secondLongNumFileName, "r");
    if (!secondLongNumFile) {
        printf("Error: Unable to open file: %s \n", secondLongNumFileName);
        return 0;
    }
    
    const char* thirdLongNumFileName = NULL;
    FILE* thirdLongNumFile = NULL;
    if (operation[0] == '^') {
        thirdLongNumFileName = argv[4];
        thirdLongNumFile = fopen(thirdLongNumFileName, "r");
        if (!thirdLongNumFile) {
            printf("Error: Unable to open file: %s \n", thirdLongNumFileName);
            return 0;
        }
        
    }
    
    const char* resultLongNumFileName = (operation[0] != '^') ? argv[4] : argv[5];
    FILE* resultLongNumFile = fopen(resultLongNumFileName, "w+");
    if (!resultLongNumFile) {
        printf("Error: Unable to open file: %s \n", resultLongNumFileName);
        return 0;
    }
    
    const char* ifBinary = (operation[0] != '^') ? argv[5] : argv[6];
    if (argc == 6 && operation[0] != '^' || argc == 7 && operation[0] == '^') {
        if (strcmp(ifBinary, "-b")) {
            printf("Error: Invalid flag: %s \n", ifBinary);
            return 0;
        }
        binary = 1;
    }
    
    //upload first large number--------------------------------------------------------------------------------------
    unsigned long long firstLongNumberSize = 0;
    unsigned long long* firstLongNumber = loadLongNumber(&firstLongNumberSize, firstLongNumFile, firstLongNumFileName);
    
    
    //upload second large number-------------------------------------------------------------------------------------
    unsigned long long secondLongNumberSize = 0;
    unsigned long long* secondLongNumber = loadLongNumber(&secondLongNumberSize, secondLongNumFile, secondLongNumFileName);
    
    
    //upload third large number optionally----------------------------------------------------------------------------
    unsigned long long thirdLongNumberSize = 0;
    unsigned long long* thirdLongNumber = NULL;
    if (operation[0] == '^') {
        thirdLongNumber = loadLongNumber(&thirdLongNumberSize, thirdLongNumFile, thirdLongNumFileName);
    }
    
    //perform chosen operation----------------------------------------------------------------------------------------
    unsigned long long resultLongNumberSize = 0;
    unsigned long long* resultLongNumber = NULL;
        
        
    switch (operation[0]) {
        case '+':
        {
            resultLongNumber = sum(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize);
            break;
        }
        case '-':
        {
            resultLongNumber = subtract(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize);
            break;
        }
        case '*':
        {
            resultLongNumber = multiply(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize);
            break;
        }
        case '/':
        {
            resultLongNumber = divide(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize);
            break;
        }
        case '%':
        {
            resultLongNumber = module(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize);
            break;
        }
        case '^':
        {
            resultLongNumber = power(&resultLongNumberSize, firstLongNumber, firstLongNumberSize, secondLongNumber, secondLongNumberSize, thirdLongNumber, thirdLongNumberSize);
            break;
        }
        default:
            break;
    }
        
    //save result long number----------------------------------------------------------------------------------------
    saveLongNumber(resultLongNumberSize, resultLongNumber, resultLongNumFile);
        
    //free allocated memory and exit---------------------------------------------------------------------------------
    free(firstLongNumber);
    free(secondLongNumber);
    free(resultLongNumber);
    return 0;
}

unsigned long long* lvlUp(unsigned long long* longNumber, unsigned long long* longNumSize) {
    unsigned long long resultLongNumberSize = *longNumSize + 1;
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, resultLongNumberSize * sizeof(unsigned long long));
        
    for (unsigned long long i = resultLongNumberSize - 1; i > 0; --i) {
        resultLongNumber[i] = longNumber[i - 1];
    }
    
    *longNumSize = resultLongNumberSize;
    return resultLongNumber;
}

unsigned char isLessOrEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize) {
    
    while (firstLongNumber[firstLongNumberSize - 1] == 0 && firstLongNumberSize > 1) {
        --firstLongNumberSize;
    }
        
    while (secondLongNumber[secondLongNumberSize - 1] == 0 && secondLongNumberSize > 1) {
        --secondLongNumberSize;
    }
        
    if (firstLongNumberSize != secondLongNumberSize) {
        return firstLongNumberSize < secondLongNumberSize;
    }
        
    for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
        if (firstLongNumber[i] != secondLongNumber[i]) {
            return firstLongNumber[i] < secondLongNumber[i];
        }
    }
    return 0;
}
    

unsigned char isEqual(unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize){
    
    while (firstLongNumber[firstLongNumberSize - 1] == 0 && firstLongNumberSize > 1) {
        --firstLongNumberSize;
    }
        
    while (secondLongNumber[secondLongNumberSize - 1] == 0 && secondLongNumberSize > 1) {
        --secondLongNumberSize;
    }
        
    if (firstLongNumberSize != secondLongNumberSize) {
        return 0;
    }
        
    for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
        if (firstLongNumber[i] != secondLongNumber[i]) {
            return 0;
        }
    }
    return 1;
}
    
    
unsigned long long* multiplyOnN(unsigned long long *resultLongNumberSize, unsigned long long* longNumber, unsigned long long longNumSize, unsigned long long n) {
        
    *resultLongNumberSize = longNumSize + 1;
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(*resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, *resultLongNumberSize * sizeof(unsigned long long));
        
    if (binary) {
        __uint128_t binaryBuffer  = 1;
        for (unsigned long long i = 0; i < *resultLongNumberSize - 1; ++i) {
            binaryBuffer *= longNumber[i];
            binaryBuffer *= n;
            binaryBuffer += resultLongNumber[i];
            resultLongNumber[i] = binaryBuffer;
            binaryBuffer >>= 64;
            resultLongNumber[i + 1] = binaryBuffer;
            binaryBuffer = 1;
        }
        
    }
    else {
        unsigned long long carry = 0;
        unsigned long long cur = 0;
        for (unsigned long long i = 0; i < *resultLongNumberSize - 1 || carry; ++i) {
            cur = carry + longNumber[i] * n;
            resultLongNumber[i] = cur % cellCapasity;
            carry = cur / cellCapasity;
        }
    }
        
    if (resultLongNumber[*resultLongNumberSize - 1] == 0) {
        --resultLongNumberSize;
    }
        
    return resultLongNumber;
}
    

unsigned long long* sum(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize){

    if (firstLongNumberSize > secondLongNumberSize) {
        *resultLongNumberSize = firstLongNumberSize + 1;
    }
    else {
        *resultLongNumberSize = secondLongNumberSize + 1;
    }
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(*resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, *resultLongNumberSize * sizeof(unsigned long long));
        
    unsigned long long curSum = 0;
    __uint128_t binaryBuffer  = 0;
    for (unsigned long long i = 0; i < *resultLongNumberSize - 1; ++i) {
        if (firstLongNumberSize > i && secondLongNumberSize > i) {
            if (binary) {
                binaryBuffer += resultLongNumber[i];
                binaryBuffer += firstLongNumber[i];
                binaryBuffer += secondLongNumber[i];
                resultLongNumber[i] = binaryBuffer;
                binaryBuffer >>= 64;
                resultLongNumber[i + 1] = binaryBuffer;
                binaryBuffer = 0;
            }
            else {
                    
                curSum = resultLongNumber[i] + firstLongNumber[i] + secondLongNumber[i];
                resultLongNumber[i] = curSum % cellCapasity;
                resultLongNumber[i + 1] = curSum / cellCapasity;
            }
        }
        else {
            if (firstLongNumberSize > i) {
                resultLongNumber[i] += firstLongNumber[i];
            }
            if (secondLongNumberSize > i) {
                resultLongNumber[i] += secondLongNumber[i];
            }
        }
    }
        
    if (resultLongNumber[*resultLongNumberSize - 1] == 0) {
        --*resultLongNumberSize;
    }
        
    return resultLongNumber;
}
    

unsigned long long* subtract(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize) {
        
    while (firstLongNumber[firstLongNumberSize - 1] == 0 && firstLongNumberSize > 1) {
        --firstLongNumberSize;
    }
        
    while (secondLongNumber[secondLongNumberSize - 1] == 0 && secondLongNumberSize > 1) {
        --secondLongNumberSize;
    }
        
    *resultLongNumberSize = firstLongNumberSize;
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(*resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, *resultLongNumberSize * sizeof(unsigned long long));
        
    for (unsigned long long i = 0; i < *resultLongNumberSize; ++i) {
        resultLongNumber[i] = firstLongNumber[i];
    }
        
    if (binary) {
        for (unsigned long long i = 0; i < secondLongNumberSize; ++i) {
            if (resultLongNumber[i] >= secondLongNumber[i]) {
                resultLongNumber[i] -= secondLongNumber[i];
            }
            else {
                resultLongNumber[i] -= secondLongNumber[i];
                --resultLongNumber[i + 1];
            }
        }
    }
    else {
        unsigned long long carry = 0;
        for (unsigned long long i = 0; i < secondLongNumberSize || carry; ++i) {
            
            resultLongNumber[i] -= carry + (i < secondLongNumberSize ? secondLongNumber[i] : 0);
            carry = resultLongNumber[i] > cellCapasity;
            if (carry) {
                resultLongNumber[i] += cellCapasity;
            }
        }
    }
    while (resultLongNumber[*resultLongNumberSize - 1] == 0 && *resultLongNumberSize > 1) {
        --*resultLongNumberSize;
    }
        
    return resultLongNumber;
}
    

unsigned long long* multiply(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize) {

    *resultLongNumberSize = firstLongNumberSize + secondLongNumberSize + 1;
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(*resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, *resultLongNumberSize * sizeof(unsigned long long));
        
    if (binary) {
        __uint128_t binaryBuffer  = 1;
        unsigned long long carry = 0;
        for (unsigned long long i = 0; i < firstLongNumberSize; ++i) {
            for (unsigned long long j = 0; j < secondLongNumberSize; ++j) {
                binaryBuffer *= firstLongNumber[i];
                binaryBuffer *= secondLongNumber[j];
                binaryBuffer += carry;
                binaryBuffer += resultLongNumber[i + j];
                resultLongNumber[i + j] = binaryBuffer;
                binaryBuffer >>= 64;
                carry = binaryBuffer;
                binaryBuffer = 1;
                if (j == secondLongNumberSize - 1) {
                    resultLongNumber[i + j + 1] += carry;
                }
            }
                
            carry = 0;
        }
    }
    else {
        unsigned long long cur = 0;
        for (unsigned long long i = 0; i < firstLongNumberSize; ++i) {
            for (unsigned long long j = 0, carry = 0; j < secondLongNumberSize || carry; ++j) {
                cur = resultLongNumber[i + j] + firstLongNumber[i] * 1 * (j < secondLongNumberSize ? secondLongNumber[j] : 0) + carry;
                resultLongNumber[i + j] = cur % cellCapasity;
                carry = cur / cellCapasity;
            }
        }
    }
        
    while (resultLongNumber[*resultLongNumberSize - 1] == 0 && *resultLongNumberSize > 1) {
        --*resultLongNumberSize;
    }
    
    return resultLongNumber;
}
    
    
unsigned long long* divide(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize) {
    
    *resultLongNumberSize = firstLongNumberSize - secondLongNumberSize + 1;
    unsigned long long* resultLongNumber = (unsigned long long*)malloc(*resultLongNumberSize * sizeof(unsigned long long));
    memset(resultLongNumber, 0, *resultLongNumberSize * sizeof(unsigned long long));
        
    unsigned long long currentLongNumSize = 0;
    unsigned long long* currentLongNum = (unsigned long long*)malloc(currentLongNumSize * sizeof(unsigned long long));
    memset(currentLongNum, 0, currentLongNumSize * sizeof(unsigned long long));
        
    unsigned long long curSize = 0;
    unsigned long long* cur;
        
    if (binary) {
        for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
            
            currentLongNum = lvlUp(currentLongNum, &currentLongNumSize);
                
            currentLongNum[0] = firstLongNumber[i];
                
            unsigned long long x = 0, l = 0;
            __uint128_t r = --x;
            ++x;
            ++r;
            while (l <= r) {
                unsigned long long m = (l + r) >> 1;
                    
                cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, m);
                    
                if (isLessOrEqual(cur, curSize, currentLongNum, currentLongNumSize)) {
                    x = m;
                    l = m + 1;
                }
                else {
                    r = m - 1;
                }
                    
            }
            resultLongNumber[i] = x;
            cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, x);
                
            currentLongNum = subtract(&currentLongNumSize, currentLongNum, currentLongNumSize, cur, curSize);
        }
        
    }
    else {
            
        for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
            currentLongNum = lvlUp(currentLongNum, &currentLongNumSize);
            
            currentLongNum[0] = firstLongNumber[i];
                
            unsigned long long x = 0, l = 0, r = cellCapasity;
            while (l <= r) {
                unsigned long long m = (l + r) >> 1;
                    
                cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, m);
                    
                if (isLessOrEqual(cur, curSize, currentLongNum, currentLongNumSize)) {
                    x = m;
                    l = m + 1;
                }
                else {
                    r = m - 1;
                }
                    
            }
            resultLongNumber[i] = x;
            cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, x);
                
            currentLongNum = subtract(&currentLongNumSize, currentLongNum, currentLongNumSize, cur, curSize);
        }
    }
        
    while (resultLongNumber[*resultLongNumberSize - 1] == 0 && *resultLongNumberSize > 1) {
        --*resultLongNumberSize;
    }
    if (binary) {
        if (isEqual(currentLongNum, currentLongNumSize, secondLongNumber, secondLongNumberSize)) {
            unsigned long long tmpLongNumSize = 1;
            unsigned long long* tmpLongNum = (unsigned long long*)malloc(tmpLongNumSize * sizeof(unsigned long long));
            tmpLongNum[0] = 1;
            resultLongNumber = sum(resultLongNumberSize, resultLongNumber, *resultLongNumberSize, tmpLongNum, tmpLongNumSize);
        }
    }
        
        
    return resultLongNumber;
}
    

unsigned long long* module(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize) {
    
    unsigned long long currentLongNumSize = 0;
    unsigned long long* currentLongNum = (unsigned long long*)malloc(currentLongNumSize * sizeof(unsigned long long));
    memset(currentLongNum, 0, currentLongNumSize * sizeof(unsigned long long));
        
    unsigned long long curSize = 0;
    unsigned long long* cur;
        
    if (binary) {
        for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
            currentLongNum = lvlUp(currentLongNum, &currentLongNumSize);
            
            currentLongNum[0] = firstLongNumber[i];
                
            unsigned long long x = 0, l = 0;
            __uint128_t r = --x;
            ++x;
            ++r;
            while (l <= r) {
                unsigned long long m = (l + r) >> 1;
                    
                cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, m);
                    
                if (isLessOrEqual(cur, curSize, currentLongNum, currentLongNumSize)) {
                    x = m;
                    l = m + 1;
                }
                else {
                    r = m - 1;
                }
                
            }
            cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, x);
                
            currentLongNum = subtract(&currentLongNumSize, currentLongNum, currentLongNumSize, cur, curSize);
        }
    }
    else {
            
        for (unsigned long long i = firstLongNumberSize - 1; i < ULONG_LONG_MAX; --i) {
            currentLongNum = lvlUp(currentLongNum, &currentLongNumSize);
                
            currentLongNum[0] = firstLongNumber[i];
                
            unsigned long long x = 0, l = 0, r = cellCapasity;
            while (l <= r) {
                unsigned long long m = (l + r) >> 1;
                    
                cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, m);
                    
                if (isLessOrEqual(cur, curSize, currentLongNum, currentLongNumSize)) {
                    x = m;
                    l = m + 1;
                }
                else {
                    r = m - 1;
                }
                    
            }
                
            cur = multiplyOnN(&curSize, secondLongNumber, secondLongNumberSize, x);
                
            currentLongNum = subtract(&currentLongNumSize, currentLongNum, currentLongNumSize, cur, curSize);
        }
    }
        
    if (binary) {
        if (isEqual(currentLongNum, currentLongNumSize, secondLongNumber, secondLongNumberSize)) {
            unsigned long long tmpLongNumSize = 1;
            unsigned long long* tmpLongNum = (unsigned long long*)malloc(tmpLongNumSize * sizeof(unsigned long long));
            tmpLongNum[0] = 0;
            *resultLongNumberSize = tmpLongNumSize;
            return tmpLongNum;
        }
    }
    
    *resultLongNumberSize = currentLongNumSize;
    return currentLongNum;
}
    

unsigned long long* power(unsigned long long* resultLongNumberSize, unsigned long long* firstLongNumber, unsigned long long firstLongNumberSize, unsigned long long* secondLongNumber, unsigned long long secondLongNumberSize, unsigned long long* thirdLongNumber, unsigned long long thirdLongNumberSize) {
    
    unsigned long long *resultLongNumber = NULL;
    unsigned long long *cur = NULL;
    unsigned long long curSize = 0;
    unsigned long long decrementSize = 1;
    unsigned long long* decrement = (unsigned long long*)malloc(decrementSize * sizeof(unsigned long long));
    decrement[0] = 1;
        
    resultLongNumber = module(resultLongNumberSize, firstLongNumber, firstLongNumberSize, thirdLongNumber,thirdLongNumberSize);
    
    if (secondLongNumberSize == 1 && secondLongNumber[0] == 1) {
        return resultLongNumber;
    }
        
    cur = multiply(&curSize, resultLongNumber, *resultLongNumberSize, resultLongNumber,*resultLongNumberSize);
        
    resultLongNumber = module(resultLongNumberSize, cur, curSize, thirdLongNumber, thirdLongNumberSize);
        
    if (secondLongNumberSize == 1 && secondLongNumber[0] == 2) {
            return resultLongNumber;
    }
        
        while ((secondLongNumber[0] - 2) != 0 || secondLongNumberSize != 1) {
            
            cur = multiply(&curSize, firstLongNumber, firstLongNumberSize, resultLongNumber,*resultLongNumberSize);
            
            resultLongNumber = module(resultLongNumberSize, cur, curSize, thirdLongNumber, thirdLongNumberSize);
            
            secondLongNumber = subtract(&secondLongNumberSize, secondLongNumber, secondLongNumberSize, decrement, decrementSize);
        }
    return resultLongNumber;
}