#!/usr/bin/python
import sys
import os
import longNumber

if len(sys.argv) < 5 :
    print "Error: Too few arguments. <first_long_number_filename> [ + | - | * | / | % | ^ ] <second_long_number_filename> <result_long_number_filename>"
    sys.exit(0)

if len(sys.argv) > 7 :
    print "Error: Too many arguments. <first_long_number_filename> [ + | - | * | / | % | ^ ] <second_long_number_filename> <result_long_number_filename> <module_long_number_filename> <-b>"
    sys.exit(0)

if not os.path.exists(sys.argv[1]):
    print "Error: Unable to open file: ", sys.argv[1]
    sys.exit(0)

if ((len(sys.argv[2]) > 1 or sys.argv[2][0] == '\0') or sys.argv[2][0] != '+' and sys.argv[2][0] != '-' and sys.argv[2][0] != '*' and sys.argv[2][0] != '/' and sys.argv[2][0] != '%' and sys.argv[2][0] != '^') :
    print "Error: Wrong operation: ", sys.argv[2][0]
    sys.exit(0)


if not os.path.isfile(sys.argv[3]) :
    print "Error: Unable to open file: ", sys.argv[3]
    sys.exit(0)

if not os.path.isfile(sys.argv[4]) :
 	file = open(sys.argv[4], 'w+')
	file.close()
	
bin = False

if len(sys.argv) == 5 :
    if sys.argv[2][0] == '^' :
        print "Error: Input module file"
        sys.exit(0)

if len(sys.argv) == 6 :
    if sys.argv[2][0] == '^' :
        if not os.path.isfile(sys.argv[5]) :
            file = open(sys.argv[5], 'w+')
            file.close()
    else :
        if sys.argv[5] != "-b" :
            print "Error: Invalid flag: ", sys.argv[5]
            sys.exit(0)
        
        bin = True

if len(sys.argv) == 7 :
    if not os.path.isfile(sys.argv[5]) :
		file = open(sys.argv[5], 'w+')
		file.close()
      
    if sys.argv[6] != "-b" :
        print "Error: Invalid flag: ", sys.argv[6]
        sys.exit(0)
    
    bin = True

a = longNumber.longNumber(sys.argv[1], bin)

b = longNumber.longNumber(sys.argv[3], bin)

result = longNumber.longNumber()

if sys.argv[2][0] == '+' :
    result = a + b

if sys.argv[2][0] == '-' :
    result = a - b

if sys.argv[2][0] == '*' :
    result = a * b

if sys.argv[2][0] == '/' :
    result = a / b

if sys.argv[2][0] == '%' :
    result = a % b

if sys.argv[2][0] == '^' :
    c = longNumber.longNumber(sys.argv[5], bin)
    result = a.powerByModule(b, c)
if len(sys.argv) == 7 :
	result.save(sys.argv[5])
else:
	result.save(sys.argv[4])